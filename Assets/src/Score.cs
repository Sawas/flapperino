﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {
	public int score { get; private set; }
	public Text text;

	void Start(){
		score = 0;
		DisplayScore ();
	}

	void DisplayScore(){
		text.text = "Score: " + score;
	}

	public void Increment(int score){
		this.score += score;
		DisplayScore ();
	}

	public string GetScore(){
		return "" + score;
	}

	public void Reset(){
		score = 0;
		DisplayScore ();
	}
}

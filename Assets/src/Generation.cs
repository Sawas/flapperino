﻿using UnityEngine;
using System.Collections;

public class Generation : MonoBehaviour {
	public GameObject pipePrefab;
	public float verticalSpaceBetweenPipes; 
	public float horizontalSpaceBetweenPipes; 
	public float heightBoundry;
	public float visiblePipeCount;
	public float startingSpace;

	void Start() {
		lastSpawnPos = gameObject.transform.position;
		//var tmp = heightBoundry;
		//heightBoundry = 8;
		for (int i = 1; i <= visiblePipeCount; i++)
			SpawnPipeSet (startingSpace + lastSpawnPos.z + i * horizontalSpaceBetweenPipes); 
		//heightBoundry = tmp;
	}

	private Vector3 lastSpawnPos;
	void Update () {
		if (lastSpawnPos.z + horizontalSpaceBetweenPipes < gameObject.transform.position.z) {
			lastSpawnPos = gameObject.transform.position;
			SpawnPipeSet (lastSpawnPos.z + visiblePipeCount * horizontalSpaceBetweenPipes);
		}
	}

	void SpawnPipeSet(float z){
		var y = Random.Range (0f, heightBoundry);

		var topPipe = Instantiate (pipePrefab);
		var botPipe = Instantiate (pipePrefab);

		botPipe.SendMessage ("SetScoreValue", 0);
		topPipe.SendMessage ("SetScoreValue", 1);
		topPipe.transform.localScale = new Vector3 (0.05f, -0.05f, 0.05f);
		botPipe.transform.position = new Vector3 (-4.0f, y, z);
		topPipe.transform.position = new Vector3 (-4.0f, (float)(y + verticalSpaceBetweenPipes + 15.5f), z); //hardcoded pipe height, TODO: fix that bs 
	}

	void Reset(){
		foreach (var obj in GameObject.FindGameObjectsWithTag ("Pipe")) {
			Destroy (obj);
		}
		Start ();
	}
}

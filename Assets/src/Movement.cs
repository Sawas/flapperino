﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {
	public float jumpForce;
	public float movementSpeed;
	public float gravity;

	private Vector3 defPos;
	private Quaternion defRot;

	void Start () {
		Physics.gravity = new Vector3 (0, gravity * -1, 0);
		defPos = transform.position;
		defRot = transform.rotation;
	}

	private bool touched = false;
	void Update () {
		SafetyReset ();
		transform.Translate (0, 0, movementSpeed * Time.deltaTime);

		if (Input.touchCount > 0 || Input.anyKey)
				GetComponent<Rigidbody> ().AddForce (Vector3.up * jumpForce);
	}

	void OnCollisionEnter (Collision collision){
		Debug.Log ("Pipe hit!");
		Reset ();
	}

	void Reset(){
		transform.position = defPos;
		transform.rotation = defRot;
		GetComponent<Rigidbody> ().velocity = Vector3.zero;
		GetComponent<Rigidbody> ().angularVelocity = Vector3.zero;
	
		GetComponentInChildren<Generation> ().BroadcastMessage ("Reset");
		GetComponentInChildren<Score> ().BroadcastMessage ("Reset");
	}

	void SafetyReset(){
		if (transform.position.y < -100 || transform.position.y > 100)
			Reset ();
	}
}

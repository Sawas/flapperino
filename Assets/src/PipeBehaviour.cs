﻿using UnityEngine;
using System.Collections;

public class PipeBehaviour : MonoBehaviour {
	private GameObject player;
	private GameObject score; 
	private int scoreValue;

	void Start(){
		player = GameObject.Find ("Player");
		score = GameObject.Find ("Score");
	}

	void Update () {
		if (player.transform.position.z - 8 > transform.position.z) { //hardcoded pipe width, TODO: fix that bs 
			score.SendMessage("Increment", scoreValue);
			Destroy (gameObject);
		}
	}

	void SetScoreValue(int scoreValue){
		this.scoreValue = scoreValue;
	}
}

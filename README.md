# Flapperino
Quick game made in a lecture on VR in National Students' Academy (NMA) hosted Residential Winter Session 2017.
Game is intended to be played with Google Cardboard.

## Screenshots
![Gameplay screenshot 1](https://i.postimg.cc/k4dW4CGj/flapperino.png)
![Gameplay screenshot 2](https://i.postimg.cc/3r12KPTB/flapperino2.png)
